build: unifont.bgf unifont_jp.bgf unifont_legacy.bgf

unifont.bgf:
	hex2bdf < /usr/share/unifont/unifont.hex > unifont.bdf
	bdftobogl -b unifont.bdf > unifont.bgf

unifont_jp.bgf:
	hex2bdf < /usr/share/unifont/unifont_jp.hex > unifont_jp.bdf
	bdftobogl -b unifont_jp.bdf > unifont_jp.bgf

unifont_legacy.bgf:
	bdftobogl -b /usr/src/unifont.bdf > unifont_legacy.bgf

clean:
	rm -f unifont.bdf unifont.bgf
	rm -f unifont_jp.bdf unifont_jp.bgf
	rm -f unifont_legacy.bgf
